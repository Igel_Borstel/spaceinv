#ifndef INCLUDE_BULLET_HPP
#define INCLUDE_BULLET_HPP

#include <SFML/Graphics.hpp>
#include "MessageSender.hpp"
#include "Listener.hpp"
#include "messages/PhysicMessage.hpp"

namespace spaceinv
{
	class Bullet : public Listener
	{
		sf::Vector2f position;
		sf::Vector2f size;
		sf::RectangleShape shape;
		MessageSender& sender;
		sf::Time oldTime;
		static sf::Time neededUpdateTime;
		bool alive;
		void handlePhysicMessage(std::shared_ptr<PhysicMessage>);
	public:
		Bullet(float, float, float, float, MessageSender&);
		virtual ~Bullet();
		void draw(sf::RenderTarget&);
		sf::Vector2f getPosition() const;
		sf::Vector2f getSize() const;
		void update(sf::Time);
		bool isAlive() const;
		virtual void handleMessage(std::shared_ptr<Message>) override;
	};

	inline sf::Vector2f Bullet::getPosition() const { return position; }
	inline sf::Vector2f Bullet::getSize() const { return size; }
	inline bool Bullet::isAlive() const { return alive; }
}

#endif // !INCLUDE_BULLET_HPP
