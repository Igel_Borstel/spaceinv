#ifndef INCLUDE_GAME_HPP
#define INCLUDE_GAME_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include "MessageSender.hpp"
#include "Player.hpp"
#include "PhysicsControl.hpp"
#include "Bullet.hpp"
#include "Listener.hpp"
#include "messages/PhysicMessage.hpp"
#include "messages/PlayerMessage.hpp"
#include "messages/GameMessage.hpp"
#include "EnemieGroup.hpp"

namespace spaceinv
{
	class Game : public Listener
	{
		enum class GameState
		{
			options = 0,
			credits,
			welcome, 
			startGame,
			gameOver,
			playerWon,
			pausing,
			playing
		} state;
		
		sf::RenderWindow gameWindow;
		unsigned int width, height;
		MessageSender messageSender;
		Player player;
		PhysicsControl physics;
		std::vector<std::shared_ptr<Bullet>> bulletVector;
		EnemieGroup enemies;
		sf::Clock gameClock;
		sf::Time oldTime;
		int points;

		void onOptions();
		void onCredits();
		void onWelcome();
		void onStartGame();
		void onGameOver();
		void onPlayerWon();
		void onPausing();
		void onPlaying();
		void updateGame();
		void renderGame();

		void handlePlayerMessage(std::shared_ptr<PlayerMessage> msg);
		void handlePhysicMessage(std::shared_ptr<PhysicMessage> msg);
		void handleGameMessage(std::shared_ptr<GameMessage> msg);
	public:
		Game();
		void loop();
		virtual void handleMessage(std::shared_ptr<Message>) override;
	};
}

#endif // !INCLUDE_GAME_HPP
