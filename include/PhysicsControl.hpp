#ifndef INCLUDE_PHYSICSCONTROL_HPP
#define INCLUDE_PHYSICSCONTROL_HPP

#include "Listener.hpp"
#include "messages/PlayerMessage.hpp"
#include "messages/GameMessage.hpp"
#include "Bullet.hpp"
#include "Enemie.hpp"
#include "EnemieGroup.hpp"
#include "MessageSender.hpp"

namespace spaceinv
{
	class Player;

	class PhysicsControl : public Listener
	{
		void handlePlayerMessage(std::shared_ptr<PlayerMessage>);
		void handleGameMessage(std::shared_ptr<GameMessage>);
		Player& player;
		sf::Time oldTime;
		std::vector<std::shared_ptr<Bullet>>& bulletVector;
		EnemieGroup& enemies;
		MessageSender& sender;
	public:
		PhysicsControl(Player&, std::vector<std::shared_ptr<Bullet>>&, EnemieGroup&, MessageSender&);
		virtual void handleMessage(std::shared_ptr<Message>) override;
		void update(sf::Time);
	};
}

#endif // !INCLUDE_PHYSICSCONTROL_HPP
