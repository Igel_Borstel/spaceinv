#ifndef INCLUDE_ENEMIEGROUP_HPP
#define INCLUDE_ENEMIEGROUP_HPP

#include <memory>
#include "Enemie.hpp"

namespace spaceinv
{
	class EnemieGroup
	{
		friend class PhysicsControl;
		std::vector<std::shared_ptr<Enemie>> enemies;
		sf::Time oldTime;
		MessageSender& sender;
		unsigned int gameWidth, gameHeight;
		float groupWidth, groupHeight;
		sf::Vector2f position;
		sf::Vector2f size;
		enum class MoveDirection
		{
			left = 0,
			right
		} direction;

		std::vector<std::shared_ptr<Enemie>>& getEnemieVector();
		void move(float);
	public:
		EnemieGroup(unsigned int, unsigned int, unsigned int, unsigned int, float, float, MessageSender&);
		void update(sf::Time);
		void draw(sf::RenderTarget&);
		sf::Vector2f getPosition() const;
		sf::Vector2f getSize() const;
	};

	inline std::vector<std::shared_ptr<Enemie>>& EnemieGroup::getEnemieVector() { return enemies; }
	inline sf::Vector2f EnemieGroup::getPosition() const
	{
		return position;
	}

	inline sf::Vector2f EnemieGroup::getSize() const
	{
		return size;
	}
}

#endif // !INCLUDE_ENEMIEGROUP_HPP
