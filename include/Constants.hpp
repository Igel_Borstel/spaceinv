#ifndef INCLUDE_CONSTANTS_HPP
#define INCLUDE_CONSTANTS_HPP

#include <SFML/System.hpp>

namespace spaceinv
{
	namespace constant
	{
		constexpr float BASE_UPDATE_SECONDS{ 0.005f };
		constexpr sf::Int64 BASE_UPDATE_TIME{ static_cast<sf::Int64>(BASE_UPDATE_SECONDS * 1000000) };
		constexpr sf::Int64 SHOOT_UPDATE_TIME{ 100 * BASE_UPDATE_TIME };
		constexpr sf::Int64 PLAYER_UPDATE_TIME{ 10 * BASE_UPDATE_TIME };
		constexpr sf::Int64 ENEMIE_UPDATE_TIME{ 10 * BASE_UPDATE_TIME };
		constexpr sf::Int64 BULLET_UPDATE_TIME{ 4 * BASE_UPDATE_TIME };

		constexpr unsigned int HORIZONTAL_ENEMIE_NUMBER{ 10 };
		constexpr unsigned int VERTICAL_ENEMIE_NUMBER{ 4 };
		constexpr float ENEMIE_WIDTH{ 25.f };
		constexpr float ENEMIE_HEIGHT{ 25.f };
	}
}

#endif // !INCLUDE_CONSTANTS_HPP
