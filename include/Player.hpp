#ifndef INCLUDE_PLAYER_HPP
#define INCLUDE_PLAYER_HPP

#include <SFML/Graphics.hpp>
#include "MessageSender.hpp"

namespace spaceinv
{
	class Player
	{
		friend class PhysicsControl;
		sf::Vector2f position;
		sf::Vector2f size;
		sf::RectangleShape shape;
		MessageSender& sender;
		void move(float);
		sf::Time oldTime;
		sf::Time oldShootTime;
	public:
		Player(float, float, float, float, MessageSender&);
		void draw(sf::RenderTarget&);
		sf::Vector2f getPosition() const;
		sf::Vector2f getSize() const;
		void update(sf::Time);
	};

	inline sf::Vector2f Player::getPosition() const { return position; }
	inline sf::Vector2f Player::getSize() const { return size; }
}

#endif // !INCLUDE_PLAYER_HPP
