#ifndef INCLUDE_ENEMIE_HPP
#define INCLUDE_ENEMIE_HPP

#include <SFML/Graphics.hpp>
#include "MessageSender.hpp"
#include "messages/PhysicMessage.hpp"

namespace spaceinv
{
	class Enemie : public Listener
	{
		bool alive;
		MessageSender& sender;
		sf::Vector2f position;
		sf::Vector2f size;
		sf::Time oldTime;
		sf::RectangleShape shape;
		void handlePhysicMessage(std::shared_ptr<PhysicMessage>);
	public:
		Enemie(float, float, float, float, MessageSender&);
		virtual ~Enemie();
		virtual void handleMessage(std::shared_ptr<Message>) override;
		void update(sf::Time);
		void draw(sf::RenderTarget&);
		sf::Vector2f getPosition() const;
		sf::Vector2f getSize() const;
		bool isAlive() const;
		void move(float);
	};

	inline sf::Vector2f Enemie::getPosition() const { return position; }
	inline sf::Vector2f Enemie::getSize() const { return size; }
	inline bool Enemie::isAlive() const { return alive; }
}

#endif // !INCLUDE_ENEMIE_HPP
