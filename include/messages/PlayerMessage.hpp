#ifndef INCLUDE_PLAYERMESSAGE_HPP
#define INCLUDE_PLAYERMESSAGE_HPP

#include "Message.hpp"

namespace spaceinv
{

	class PlayerMessage : public Message
	{
	public:
		enum class MessageType
		{
			unknow = 0,
			movedRight,
			movedLeft,
			shoot
		};
	private:
		MessageType type{MessageType::unknow};
	public:
		PlayerMessage() = default;
		PlayerMessage(MessageType);

		MessageType getMessageType() const;
	};

	inline PlayerMessage::PlayerMessage(PlayerMessage::MessageType mt)
		: type{mt}
	{ }

	inline PlayerMessage::MessageType PlayerMessage::getMessageType() const { return type; }
}

#endif // !INCLUDE_PLAYER_HPP
