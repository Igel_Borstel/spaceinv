#ifndef INCLUDE_PHYSICMESSAGE_HPP
#define INCLUDE_PHYSICMESSAGE_HPP

#include <memory>
#include "Message.hpp"


namespace spaceinv
{
	class Bullet;
	class Enemie;

	class PhysicMessage : public Message
	{
	public:
		enum class MessageType
		{
			unknow=0,
			bulletDestroyed,
			enemieDestroyed,
			playerDestroyed,
			enemieGroupCollidesWithEdge
		};
	private:
		MessageType type;
		std::shared_ptr<Enemie> enemiePtr;
		std::shared_ptr<Bullet> bulletPtr;
	public:
		PhysicMessage() = default;
		PhysicMessage(MessageType, std::shared_ptr<Enemie>, std::shared_ptr<Bullet>);

		MessageType getMessageType() const;
		std::shared_ptr<Enemie> getEnemiePtr() const;
		std::shared_ptr<Bullet> getBulletPtr() const;
	};

	inline PhysicMessage::PhysicMessage(PhysicMessage::MessageType mt, std::shared_ptr<Enemie> enPtr, std::shared_ptr<Bullet> buPtr)
		: type{ mt }, enemiePtr{ enPtr }, bulletPtr{ buPtr }
	{}

	inline PhysicMessage::MessageType PhysicMessage::getMessageType() const { return type; }
	inline std::shared_ptr<Enemie> PhysicMessage::getEnemiePtr() const { return enemiePtr; }
	inline std::shared_ptr<Bullet> PhysicMessage::getBulletPtr() const { return bulletPtr; }
}

#endif // !INCLUDE_PHYSICMESSAGE_HPP
