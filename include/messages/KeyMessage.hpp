#ifndef INCLUDE_KEYMESSAGE_HPP
#define INCLUDE_KEYMESSAGE_HPP

#include <SFML/Window.hpp>
#include "Message.hpp"

namespace spaceinv
{
	class KeyMessage : public Message
	{
	public:
		enum class MessageType
		{
			unknow = 0,
			keyPressed,
			keyReleased,
		};
	private:
		MessageType messageType{ MessageType::unknow };
		sf::Keyboard::Key key;
		bool alt{ false };
		bool control{ false };
		bool shift{ false };
		bool system{ false };
	public:
		KeyMessage() = default;
		KeyMessage(MessageType, sf::Keyboard::Key, bool, bool, bool, bool);

		MessageType getMessageType() const;
		sf::Keyboard::Key getKey() const;
		bool isAltKeyPressed() const;
		bool isControlKeyPressed() const;
		bool isShiftKeyPressed() const;
		bool isSystemKeyPressed() const;
	};

	inline KeyMessage::KeyMessage(KeyMessage::MessageType type, sf::Keyboard::Key k, bool a, bool ctrl, bool sh, bool sys)
		: messageType(type), key(k), alt(a), control(ctrl), shift(sh), system(sys)	{}

	inline KeyMessage::MessageType KeyMessage::getMessageType() const {	return messageType;	}
	inline sf::Keyboard::Key KeyMessage::getKey() const { return key; }
	inline bool KeyMessage::isAltKeyPressed() const { return alt; }
	inline bool KeyMessage::isControlKeyPressed() const { return control; }
	inline bool KeyMessage::isShiftKeyPressed() const { return shift; }
	inline bool KeyMessage::isSystemKeyPressed() const { return system; }
}


#endif // !INCLUDE_KEYMESSAGE_HPP
