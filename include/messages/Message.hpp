#ifndef INCLUDE_MESSAGE_HPP
#define INCLUDE_MESSAGE_HPP

namespace spaceinv
{
	struct Message
	{
		virtual ~Message() = default;
	};
}

#endif //INCLUDE_MESSAGE_HPP