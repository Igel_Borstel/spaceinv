#ifndef INCLUDE_MESSAGES_GAMEMESSAGE_HPP
#define INCLUDE_MESSAGES_GAMEMESSAGE_HPP

#include "Message.hpp"

namespace spaceinv
{
	class GameMessage : public Message
	{
	public:
		enum class MessageType
		{
			unknow = 0,
			allEnemiesDestroyed,
			enemieGroupMoved
		};
	private:
		MessageType type{MessageType::unknow};
	public:
		GameMessage() = default;
		GameMessage(MessageType);

		MessageType getMessageType() const;
	};

	inline GameMessage::GameMessage(GameMessage::MessageType mt)
		: type{mt}
	{}

	inline GameMessage::MessageType GameMessage::getMessageType() const { return type; }
}

#endif // !INCLUDE_MESSAGES_GAMEMESSAGE_HPP
