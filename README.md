Dieses Projekt ist mal wieder ein Übungsprojekt.
Folgende Ziele sollen in diesem Projekt umgesetzt werden:
* Messagesystem (Observer)
* Ressourcemanagement

Folgende Ziele sind optional:
* Androidport
* Bestenliste über DB


Dependencies
* SFML-Window-2.3.2
* SFML-Graphics-2.3.2
* SFML-System-2.3.2