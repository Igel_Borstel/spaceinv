#include "../include/Bullet.hpp"
#include "../include/Constants.hpp"

namespace spaceinv
{
	void Bullet::handlePhysicMessage(std::shared_ptr<PhysicMessage> msg)
	{
		if (msg->getMessageType() == PhysicMessage::MessageType::bulletDestroyed)
		{
			if (msg->getBulletPtr().get() == this)
			{
				alive = false;
			}
		}
	}

	Bullet::Bullet(float x, float y, float width, float height, MessageSender& ms)
		: position{x, y}, size{width, height}, sender{ms}
	{
		shape.setSize(size);
		shape.setOrigin(sf::Vector2f{size.x / 2, size.y / 2});
		shape.setPosition(position);
		shape.setFillColor(sf::Color::Red);
	}

	Bullet::~Bullet()
	{
		if (sender.isRegistered(this))
		{
			sender.unregisterListener(this);
		}
	}

	void Bullet::draw(sf::RenderTarget& rt)
	{
		shape.setPosition(position);
		rt.draw(shape);
	}

	void Bullet::update(sf::Time elapsedTime)
	{
		sf::Time delta = elapsedTime - oldTime;
		if (delta.asMicroseconds() >= constant::BULLET_UPDATE_TIME)
		{
			position.y -= 10;
			oldTime = elapsedTime;
		}
	}

	void Bullet::handleMessage(std::shared_ptr<Message> msg)
	{
		if (typeid(*msg) == typeid(PhysicMessage))
		{
			handlePhysicMessage(std::dynamic_pointer_cast<PhysicMessage>(msg));
		}
	}

}