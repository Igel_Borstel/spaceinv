#include <typeinfo>
#include "../include/MessageSender.hpp"


namespace spaceinv
{
	void MessageSender::registerListener(Listener* listener, std::unique_ptr<Message> msg)
	{
		listenerAddressMap.insert(std::pair<Listener*, std::multimap<std::string, Listener*>::iterator>(listener, listenerMap.insert(std::pair<std::string, Listener*>(typeid(*msg).name(), listener))));
	}

	void MessageSender::unregisterListener(Listener* listener)
	{
		auto pairs = listenerAddressMap.equal_range(listener);
		for (auto iterator = pairs.first; iterator != pairs.second; ++iterator)
		{
			listenerMap.erase(iterator->second);
		}
		listenerAddressMap.erase(listener);
	}

	void MessageSender::send(std::shared_ptr<Message> msg) const
	{
		auto pairs = listenerMap.equal_range(typeid(*msg).name());
		for (auto iterator = pairs.first; iterator != pairs.second; ++iterator)
		{
			iterator->second->handleMessage(msg);
		}
	}

	bool MessageSender::isRegistered(Listener* listener) const
	{
		return (listenerAddressMap.find(listener) != listenerAddressMap.end()) ? true : false;
	}
}