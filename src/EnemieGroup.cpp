#include "../include/EnemieGroup.hpp"
#include "../include/messages/GameMessage.hpp"
#include "../include/Constants.hpp"
#include <iostream>

namespace spaceinv
{
	EnemieGroup::EnemieGroup(unsigned int gw, unsigned int gh, unsigned int eh, unsigned int ev, float enemieWidth, float enemieHeight, MessageSender& send)
		: gameWidth{gw}, gameHeight{gh}, sender{send}, direction{ MoveDirection::left }
	{
		float groupHeight = 0.5f * gameHeight;
		float groupWidth = gameWidth;


		float verticalDistance = enemieHeight + 0.5f * enemieHeight;
		float horizontalDistance = enemieWidth + 0.5f * enemieWidth;

		float verticalEdgeDistance = 0.5f * (0.5f * groupHeight - (ev * (verticalDistance)));
		float horizontalEdgeDistance = 0.5f * (groupWidth - (eh * (horizontalDistance)));

		
		size = sf::Vector2f{ (horizontalDistance * eh) /*- (0.5f * enemieWidth))*/, ((verticalDistance * ev) - (0.5f * enemieHeight)) };
		position = sf::Vector2f{ gameWidth * 0.5f, size.y * 0.5f + verticalEdgeDistance };
		for (unsigned int i = 0; i < ev; ++i)
		{
			for (unsigned int j = 0; j < eh; ++j)
			{
				float xpos = horizontalEdgeDistance + (j + 1)  * horizontalDistance;
				float ypos = verticalEdgeDistance + (i + 1) * verticalDistance;

				enemies.push_back(std::make_shared<Enemie>(xpos, ypos, enemieWidth, enemieHeight, sender));
				sender.registerListener(enemies.at(enemies.size() - 1).get(), std::make_unique<PhysicMessage>());
			}
		}
	}

	void EnemieGroup::update(sf::Time elapsedTime)
	{
		sf::Time delta = elapsedTime - oldTime;
		if(delta.asMicroseconds() >= constant::ENEMIE_UPDATE_TIME)
		{
			if(direction == MoveDirection::left)
			{
				move(-10.f);
				sender.send(std::make_shared<GameMessage>(GameMessage::MessageType::enemieGroupMoved));
			}
			else
			{
				move(10.f);
				sender.send(std::make_shared<GameMessage>(GameMessage::MessageType::enemieGroupMoved));
			}
			oldTime = elapsedTime;
			for(auto enemie : enemies)
			{
				enemie->update(elapsedTime);
			}
			size_t i = 0;
			std::vector<std::vector<std::shared_ptr<Enemie>>::iterator> enemieIterators;
			for(std::vector<std::shared_ptr<Enemie>>::iterator iter = enemies.begin(); iter != enemies.end(); ++iter, ++i)
			{
				if(enemies.at(i)->isAlive())
				{
					enemies.at(i)->update(elapsedTime);
				}
				else
				{
					//Makiere Iterator zum l�schen
					enemieIterators.push_back(iter);
				}
			}
			for(auto iter : enemieIterators)
			{
				enemies.erase(iter);
			}
			enemies.shrink_to_fit();

			if(enemies.size() == 0)
			{
				sender.send(std::make_shared<GameMessage>(GameMessage::MessageType::allEnemiesDestroyed));
			}
		}
	}

	void EnemieGroup::draw(sf::RenderTarget& rt)
	{
		for(auto enemie : enemies)
		{
			enemie->draw(rt);
		}
	}

	void EnemieGroup::move(float offset)
	{
		position.x += offset;
		for(auto enemie : enemies)
		{
			enemie->move(offset);
		}
	}
}