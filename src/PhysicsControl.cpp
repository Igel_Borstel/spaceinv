#include "../include/PhysicsControl.hpp"
#include "../include/Player.hpp"
#include "../include/Constants.hpp"

spaceinv::PhysicsControl::PhysicsControl(Player& p, std::vector<std::shared_ptr<Bullet>>& bullets, EnemieGroup& enemieGroup, MessageSender& send)
	: player{ p }, bulletVector{ bullets }, enemies{ enemieGroup }, sender{ send }
{ }

void spaceinv::PhysicsControl::handlePlayerMessage(std::shared_ptr<PlayerMessage> msg)
{
	//TODO Magic Numbers ersetzen
	if (player.getPosition().x < (player.getSize().x / 2 + 0))
	{
		player.move(10);
	}
	else if (player.getPosition().x > (750 - player.getSize().x / 2))
	{
		player.move(-10);
	}
}

void spaceinv::PhysicsControl::handleGameMessage(std::shared_ptr<GameMessage> msg)
{
	//TODO Magic Numbers entfernen
	if(enemies.getPosition().x < (enemies.getSize().x / 2 + 0))
	{
		enemies.move(10);
		enemies.direction = EnemieGroup::MoveDirection::right;
	}
	else if(enemies.getPosition().x > (750 - (enemies.getSize().x / 2)))
	{
		enemies.move(-10);
		enemies.direction = EnemieGroup::MoveDirection::left;
	}
}

void spaceinv::PhysicsControl::handleMessage(std::shared_ptr<Message> msg)
{
	if (typeid(*msg) == typeid(PlayerMessage))
	{
		handlePlayerMessage(std::dynamic_pointer_cast<PlayerMessage>(msg));
	}
	else if(typeid(*msg) == typeid(GameMessage))
	{
		handleGameMessage(std::dynamic_pointer_cast<GameMessage>(msg));
	}
}

void spaceinv::PhysicsControl::update(sf::Time elapsedTime)
{
	sf::Time delta = elapsedTime - oldTime;
	if (delta.asMicroseconds() >= constant::BASE_UPDATE_TIME)
	{
		oldTime = elapsedTime;
		for (std::shared_ptr<Bullet> bullet : bulletVector)
		{
			if (bullet->getPosition().y < (bullet->getSize().y / 2))
			{
				sender.send(std::make_shared<PhysicMessage>(PhysicMessage::MessageType::bulletDestroyed, nullptr, bullet));
			}
			else
			{
				for (std::shared_ptr<Enemie> enemie : enemies.getEnemieVector())
				{
					if ((bullet->getPosition().x >= (enemie->getPosition().x - enemie->getSize().x / 2)) &&
						(bullet->getPosition().x <= (enemie->getPosition().x + enemie->getSize().x / 2)) &&
						(bullet->getPosition().y >= (enemie->getPosition().y - enemie->getSize().y / 2)) &&
						(bullet->getPosition().y <= (enemie->getPosition().y + enemie->getSize().y / 2)))
					{
						sender.send(std::make_shared<PhysicMessage>(PhysicMessage::MessageType::enemieDestroyed, enemie, nullptr));
						sender.send(std::make_shared<PhysicMessage>(PhysicMessage::MessageType::bulletDestroyed, nullptr, bullet));
						break;
					}
				}
			}

		}
	}
}
