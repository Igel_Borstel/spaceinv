#include "../include/Game.hpp"
#include "../include/messages/KeyMessage.hpp"
#include "../include/messages/PlayerMessage.hpp"
#include "../include/messages/PhysicMessage.hpp"
#include "../include/Constants.hpp"
#include <iostream>

namespace spaceinv
{
	Game::Game()
		: state{ GameState::welcome }, width{ 750 }, height{ 375 }, player{ 375.0f, 372.5f, 50.f, 10.f, messageSender }, physics(player, bulletVector, enemies, messageSender), points{ 0 }, 
		enemies{width, height, constant::HORIZONTAL_ENEMIE_NUMBER, constant::VERTICAL_ENEMIE_NUMBER, 25.f, 25.f, messageSender}
	{
		gameClock.restart();
		gameWindow.create(sf::VideoMode{width, height}, "SpaceInv - Ein SpaceInvaders Clone");
		// TODO nicht spielend starten
		state = GameState::playing;

		//F�ge Feinde hinzu:
		//TODO magic numbers entfernen; bessere Feindverteilung �berlegen
		
		messageSender.registerListener(&physics, std::make_unique<PlayerMessage>());
		messageSender.registerListener(&physics, std::make_unique<GameMessage>());
		messageSender.registerListener(this, std::make_unique<PlayerMessage>());
		messageSender.registerListener(this, std::make_unique<PhysicMessage>());
		messageSender.registerListener(this, std::make_unique<GameMessage>());
	}

	void Game::loop()
	{
		while (gameWindow.isOpen())
		{
			switch (state)
			{
			case GameState::options:
				onOptions();
				break;
			case GameState::credits:
				onCredits();
				break;
			case GameState::welcome:
				onWelcome();
				break;
			case GameState::startGame:
				onStartGame();
				break;
			case GameState::gameOver:
				onGameOver();
				break;
			case GameState::playerWon:
				onPlayerWon();
				break;
			case GameState::pausing:
				onPausing();
				break;
			case GameState::playing:
				onPlaying();
				break;
			}
		}
	}

	void Game::handleMessage(std::shared_ptr<Message> msg)
	{
		if(typeid(*msg) == typeid(PlayerMessage))
		{
			handlePlayerMessage(std::dynamic_pointer_cast<PlayerMessage>(msg));
		}
		else if (typeid(*msg) == typeid(PhysicMessage))
		{
			handlePhysicMessage(std::dynamic_pointer_cast<PhysicMessage>(msg));
		}
		else if(typeid(*msg) == typeid(GameMessage))
		{
			handleGameMessage(std::dynamic_pointer_cast<GameMessage>(msg));
		}
	}

	void Game::onOptions()
	{
	}

	void Game::onCredits()
	{
	}

	void Game::onWelcome()
	{
	}

	void Game::onStartGame()
	{
	}

	void Game::onGameOver()
	{
	}

	void Game::onPlayerWon()
	{
		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				gameWindow.close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape)
				{
					// TODO  Fenster nicht schlie�en sonder Spielstatus auf pausieren �ndern
					gameWindow.close();
				}
				break;
			}
		}
	}

	void Game::onPausing()
	{
	}

	void Game::onPlaying()
	{
		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::LostFocus:
				//state = GameState::pausing;
				break;
			case sf::Event::Closed:
				gameWindow.close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape)
				{
					// TODO  Fenster nicht schlie�en sonder Spielstatus auf pausieren �ndern
					gameWindow.close();
				}
				else
				{
					messageSender.send(std::make_shared<KeyMessage>(KeyMessage::MessageType::keyPressed, event.key.code, event.key.alt, event.key.control, event.key.shift, event.key.system));
				}
				break;
			case sf::Event::KeyReleased:
				messageSender.send(std::make_shared<KeyMessage>(KeyMessage::MessageType::keyReleased, event.key.code, event.key.alt, event.key.control, event.key.shift, event.key.system)); 
				break;
			}
		}
		updateGame();
		renderGame();
	}

	void Game::updateGame()
	{
		sf::Time elapsedTime = gameClock.getElapsedTime();
		size_t i = 0;
		std::vector<std::vector<std::shared_ptr<Bullet>>::iterator> bulletIterators;
		for (std::vector<std::shared_ptr<Bullet>>::iterator iter = bulletVector.begin(); iter != bulletVector.end(); ++iter, ++i)
		{
			if (bulletVector.at(i)->isAlive())
			{
				bulletVector.at(i)->update(elapsedTime);
			}
			else
			{
				//Makiere Iterator zum l�schen
				bulletIterators.push_back(iter);
			}
		}
		for (auto iter : bulletIterators)
		{
			bulletVector.erase(iter);
		}
		bulletVector.shrink_to_fit();
		i = 0;

		enemies.update(elapsedTime);
		player.update(elapsedTime);
		physics.update(elapsedTime);

	}

	void Game::renderGame()
	{
		gameWindow.clear(sf::Color::White);
		player.draw(gameWindow);
		for (std::shared_ptr<Bullet> bullet : bulletVector)
		{
			bullet->draw(gameWindow);
		}

		enemies.draw(gameWindow);
		gameWindow.display();
	}

	void Game::handlePlayerMessage(std::shared_ptr<PlayerMessage> msg)
	{
		if(msg->getMessageType() == PlayerMessage::MessageType::shoot)
		{
			bulletVector.push_back(std::make_shared<Bullet>(player.getPosition().x, player.getPosition().y, 2, 10, messageSender));
			messageSender.registerListener(bulletVector.at(bulletVector.size() - 1).get(), std::make_unique<PhysicMessage>());
		}
	}

	void Game::handlePhysicMessage(std::shared_ptr<PhysicMessage> msg)
	{
		if (msg->getMessageType() == PhysicMessage::MessageType::enemieDestroyed)
		{
			points += 100;
		}
	}

	void Game::handleGameMessage(std::shared_ptr<GameMessage> msg)
	{
		if(msg->getMessageType() == GameMessage::MessageType::allEnemiesDestroyed)
		{
			state = GameState::playerWon;
			std::cout << "Spiel gewonnen! Punktestand: " << points << std::endl;
		}
	}
}