#include "../include/Player.hpp"
#include "../include/messages/PlayerMessage.hpp"
#include "../include/Constants.hpp"


void spaceinv::Player::move(float offset)
{
	position.x += offset;
}

spaceinv::Player::Player(float x, float y, float width, float height, MessageSender& send)
	: position{x,y}, size{width, height}, sender{send}
{
	shape.setSize(size);
	shape.setOrigin(sf::Vector2f{size.x / 2, size.y / 2});
	shape.setPosition(position);
	shape.setFillColor(sf::Color::Blue);
}

void spaceinv::Player::draw(sf::RenderTarget& rt)
{
	shape.setPosition(position);
	rt.draw(shape);
}

void spaceinv::Player::update(sf::Time elapsedTime)
{
	sf::Time delta = elapsedTime - oldTime;
	if (delta.asMicroseconds() >= constant::PLAYER_UPDATE_TIME)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left))
		{
			move(-10.f);
			sender.send(std::make_shared<PlayerMessage>(PlayerMessage::MessageType::movedLeft));
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right))
		{
			move(10.f);
			sender.send(std::make_shared<PlayerMessage>(PlayerMessage::MessageType::movedRight));
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
		{
			sf::Time deltaShoot = elapsedTime - oldShootTime;
			if(deltaShoot.asMicroseconds() >= constant::SHOOT_UPDATE_TIME)
			{
				sender.send(std::make_shared<PlayerMessage>(PlayerMessage::MessageType::shoot));
				oldShootTime = elapsedTime;
			}
		}
		oldTime = elapsedTime;
	}
}


