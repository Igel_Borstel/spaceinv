#include "../include/Enemie.hpp"
#include "../include/Constants.hpp"

namespace spaceinv
{
	void Enemie::handlePhysicMessage(std::shared_ptr<PhysicMessage> msg)
	{
		if (msg->getMessageType() == PhysicMessage::MessageType::enemieDestroyed)
		{
			if (msg->getEnemiePtr().get() == this)
			{
				alive = false;
			}
		}
	}

	Enemie::Enemie(float x, float y, float width, float height, MessageSender& ms)
		: position{ x, y }, size{ width, height }, sender{ ms }
	{
		shape.setSize(size);
		shape.setOrigin(sf::Vector2f{ size.x / 2, size.y / 2 });
		shape.setPosition(position);
		shape.setFillColor(sf::Color::Green);
	}

	Enemie::~Enemie()
	{
		if (sender.isRegistered(this))
		{
			sender.unregisterListener(this);
		}
	}

	void Enemie::handleMessage(std::shared_ptr<Message> msg)
	{
		if (typeid(*msg) == typeid(PhysicMessage))
		{
			handlePhysicMessage(std::dynamic_pointer_cast<PhysicMessage>(msg));
		}
	}

	void Enemie::update(sf::Time elapsedTime)
	{
		sf::Time delta = elapsedTime - oldTime;
		if (delta.asMicroseconds() >= constant::ENEMIE_UPDATE_TIME)
		{
			oldTime = elapsedTime;
		}
	}

	void Enemie::draw(sf::RenderTarget& rt)
	{
		shape.setPosition(position);
		rt.draw(shape);
	}

	void Enemie::move(float offset)
	{
		position.x += offset;
	}
}